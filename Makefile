# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/05/24 18:24:48 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = minishell.a\

#CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)
CFLAGS = -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = main.c minishell.c ft_options.c ft_init.c ft_recup_options.c\
	  ft_display_error.c ft_display_version.c ft_display_help.c\
	  ft_destruct_minishell.c  ft_add_env.c ft_replace_cara.c\
	  ft_init_bulletins.c ft_exit.c ft_cd.c ft_env.c ft_setenv.c\
	  ft_unsetenv.c ft_read_input.c ft_cmd.c ft_del_env.c ft_tree_cmd.c\
	  ft_find_path.c ft_parcours_cmd.c ft_rotate_cmd.c ft_get_env.c\
	  ft_pars.c ft_init_operande.c ft_init_input.c ft_del_tree.c\
	  ft_del_info_et_input.c ft_add_input.c ft_create_input.c\
	  ft_lst_input.c ft_print_env.c ft_replace_home.c ft_replace_cd_pwd.c\
	  ft_operande.c ft_fin_commande.c ft_et_commande.c ft_display_prompt.c\
	  ft_ou_commande.c

SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)
CC = gcc
all : $(NAME)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
	gcc -ltermcap -o $(NAME) $(NAME_LIB) libft/libft.a

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean fclean re
