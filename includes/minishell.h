/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 20:40:15 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 17:52:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include <string.h>
# include <sys/stat.h>
# define STDIN 0

/*
**	OPTIONS DEFINES
*/
# define C 1
# define VERSION 2
# define INIT 3
# define HELP 4

/*
** ERROR DEFINES
*/
# define INVALID_ARG -1
# define NOFILE 1
# define PERMDENIED 2
# define NODIR 3
# define ERROROCCUR 4
# define NOPWD 5

/*
** Taille de la structure bulletin
*/

# define T_BULLETIN 7
# define T_OPERATEUR 7

/*
** Retour des commandes lancer pour la gestion des || &&
*/

# define NEWCMD -1
# define FALSE 0
# define TRUE 1

/*
** Liste des cmds avec leurs args
*/

typedef struct		s_input
{
	char			**tab;
	int				op;
	struct s_input	*next;
}					t_input;

typedef struct		s_info_input
{
	size_t			size;
	t_input			*begin;
	t_input			*end;
}					t_info_input;

typedef struct		s_mini
{
	char					options;
	char					**env;
	size_t					size_env;
	struct s_bulletin		*bulletin;
	struct s_operateur		*operateur;
	struct s_cmd			*cmd;
	t_info_input			*info;
}					t_mini;

/*
** Ponteurs sur fonctions des operateurs;
*/

typedef struct		s_operateur
{
	char			*operateur;
	t_input			*(*op)(t_mini *, t_input *, int *);
	size_t			size;
}					t_operateur;

/*
** Arbre binaire equilibre qui va contenir les chemins des binaires
** I faut gerer la suppresion et l'ajout orsque le PATH est modifier
*/

typedef struct		s_cmd
{
	struct s_cmd	*n_rigth;
	struct s_cmd	*n_left;
	struct s_cmd	*parent;
	int				equilibre;
	char			*cmd;
	char			*path;
}					t_cmd;

typedef struct		s_bulletin
{
	char			*bulletin;
	int				(*f)(char **, t_mini *);
}					t_bulletin;

/*
*******************************************************************************
**								INIT										 **
*******************************************************************************
*/

void				ft_init_minishell(t_mini **mini, char **environ);
void				ft_init_env(t_mini *mini, char **environ);
t_bulletin			*ft_init_bulletins(void);
void				ft_find_path(t_mini *mini);
void				ft_create_cmd(const char *bin, t_mini *mini, char *name);
t_operateur			*ft_init_operateur(void);
t_info_input		*ft_init_info_input(void);
t_input				*ft_init_lst_input(char **tab,
					t_info_input *info, int operande);
void				ft_set_pwd(t_mini *mini);

/*
*******************************************************************************
**								OPTIONS										 **
*******************************************************************************
*/

void				ft_options(int argc, char **argv, t_mini *mini);
int					ft_recup_options(char *str, t_mini *mini);
void				ft_c_options(t_mini *mini, char *argv);

/*
*******************************************************************************
**								INPUT										 **
*******************************************************************************
*/

void				ft_recup_input(const char *line, t_mini *mini);
void				ft_launch_cmd(t_mini *mini);
char				*ft_add_elem(t_mini *mini, const char *line);
void				ft_create_input(t_mini *mini, size_t size, const char *str);
void				ft_lst_add_input(t_mini *mini, char **tab, int operande);
int					ft_verif_line(t_mini *mini);
char				*ft_replace_home(char *str, t_mini *mini);
char				*ft_replace_cara(char *str);
void				ft_pars_line(t_mini *mini);
int					ft_empty_line(const char *line);

/*
*******************************************************************************
**								EXIT										 **
*******************************************************************************
*/

int					ft_exit(char **tab, t_mini *mini);

/*
*******************************************************************************
**								CD											 **
*******************************************************************************
*/

int					ft_cd(char **tab, t_mini *mini);
char				*ft_replace_dd_pwd(char *pwd, char *replace, t_mini *mini);
int					ft_init_pwd(t_mini *mini);

/*
*******************************************************************************
**								env											 **
*******************************************************************************
*/

int					ft_env(char **tab, t_mini *mini);
char				*ft_get_env(char *str, t_mini *mini);
char				*ft_get_name_env(char *str, t_mini *mini);
int					ft_printenv(char **tab, t_mini *mini);
void				ft_print_all_env(t_mini *mini);
int					ft_search_path(char *line, t_mini *mini);

/*
*******************************************************************************
**								set_env										 **
*******************************************************************************
*/

int					ft_setenv(char **tab, t_mini *mini);
char				**ft_realloc_env(char **env, t_mini *mini, char *add_env);

/*
*******************************************************************************
**								unsetenv									 **
*******************************************************************************
*/

int					ft_unsetenv(char **tab, t_mini *mini);
char				**ft_del_env(char **env, t_mini *mini, char *del_env);

/*
*******************************************************************************
**								FILE										 **
*******************************************************************************
*/

void				ft_import_file(t_mini *mini, char *file);

/*
*******************************************************************************
**								MINISHELL									 **
*******************************************************************************
*/

int					ft_minishell(t_mini *mini);

/*
*******************************************************************************
**								CMD      									 **
*******************************************************************************
*/
int					ft_cmd(char **line, t_mini *mini, int env);
void				ft_parcours_cmd(t_cmd *cmd);
t_cmd				*ft_search_cmd(t_cmd *cmd, char *line);
void				ft_rotate_simple_left(t_cmd *cmd, t_mini *mini);
void				ft_rotate_simple_rigth(t_cmd *cmd, t_mini *mini);
void				ft_create_root(const char *path, t_mini *mini,
		const char *name);
void				ft_add_leaf(t_cmd *cmd, t_cmd *new, t_mini *mini);
t_cmd				*ft_create_node(t_cmd *cmd,
		const char *path, const char *name);
t_cmd				*ft_search_begin_words(t_cmd *cmd,
		char *line, size_t size_line);
char				*ft_lst_add_operateur(t_mini *mini,
		int indice, const char *line);
int					ft_built_in_or_cmd(char **tab, t_mini *mini, int env);
/*
*******************************************************************************
**								Operande
*******************************************************************************
*/
t_input				*ft_minishell_launch_op(int *etat,
		t_input *input, t_mini *mini);
t_input				*ft_fin_commande(t_mini *mini, t_input *input, int *etat);
t_input				*ft_et_commande(t_mini *mini, t_input *input, int *etat);
t_input				*ft_ou_commande(t_mini *mini, t_input *input, int *etat);
/*
*******************************************************************************
**								Display ERROR								 **
*******************************************************************************
*/
void				ft_display_error(int error);
int					ft_display_error_cd(int error,
		char *path, struct stat *buf);
int					ft_display_error_exec(int error, char *path);
void				ft_display_error_fils(int error, char *cmd);
/*
*******************************************************************************
**								Display										 **
*******************************************************************************
*/
void				ft_display_version(void);
void				ft_display_help(void);
void				ft_display_prompt(t_mini *mini);
/*
*******************************************************************************
**								DESTRUCT       								 **
*******************************************************************************
*/
void				ft_destruct_minishell(t_mini *mini);
void				ft_del_tree(t_cmd *cmd);
void				ft_free_operateur(t_operateur *op);
void				ft_free_input(t_input *input);
void				ft_free_info(t_info_input *info_input);
void				ft_reinit_input(t_mini *mini);
#endif
