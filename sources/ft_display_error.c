/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:33:07 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 17:30:10 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <stdlib.h>
#include <sys/stat.h>

void	ft_display_error(int error)
{
	ft_putstr_fd("minishell: ", 2);
	if (error == INVALID_ARG)
		ft_putendl_fd("Invalid arguments", 2);
}

int		ft_display_error_cd(int error, char *path, struct stat *buf)
{
	ft_putstr_fd("cd: ", 2);
	if (error == NOFILE)
		ft_putstr_fd("no such file or directory: ", 2);
	else if (error == PERMDENIED)
		ft_putstr_fd("permission denied: ", 2);
	else if (error == NODIR)
		ft_putstr_fd("not a directory: ", 2);
	else if (error == NOPWD)
		ft_putstr_fd("env PWD doesn't exist: ", 2);
	if (path)
		ft_putstr_fd(path, 2);
	ft_putchar_fd('\n', 2);
	if (buf)
		free(buf);
	return (FALSE);
}

int		ft_display_error_exec(int error, char *path)
{
	ft_putstr_fd("mini: ", 2);
	if (error == NOFILE)
		ft_putstr_fd("no such file or directory: ", 2);
	else if (error == PERMDENIED)
		ft_putstr_fd("permission denied: ", 2);
	if (path)
		ft_putstr_fd(path, 2);
	ft_putchar_fd('\n', 2);
	return (FALSE);
}

void	ft_display_error_fils(int error, char *cmd)
{
	if (error == 11)
	{
		ft_putstr_fd("mini: segmantation fault ", 2);
		ft_putendl_fd(cmd, 2);
	}
	else if (error == 6)
	{
		ft_putstr_fd("mini: abort	", 2);
		ft_putendl_fd(cmd, 2);
	}
	else if (error == 10)
	{
		ft_putstr_fd("mini: bus error	", 2);
		ft_putendl_fd(cmd, 2);
	}
	else
		ft_putendl("");
}
