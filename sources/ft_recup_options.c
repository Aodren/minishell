/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup_options.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:10:11 by abary             #+#    #+#             */
/*   Updated: 2016/04/20 18:47:11 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int		ft_recup_options(char *str, t_mini *mini)
{
	if (!ft_strcmp(str, "-c"))
	{
		mini->options = C;
		return (1);
	}
	else if (!ft_strcmp(str, "-version"))
	{
		mini->options = VERSION;
		return (1);
	}
	else if (!ft_strcmp(str, "-file"))
	{
		mini->options = INIT;
		return (1);
	}
	else if (!ft_strcmp(str, "--help"))
	{
		mini->options = HELP;
		return (1);
	}
	return (INVALID_ARG);
}
