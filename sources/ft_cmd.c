/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 20:29:08 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 18:26:11 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <unistd.h>
#include <sys/wait.h>
#include "libft.h"
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>

static void	ft_kill(int s)
{
	s = 0;
	exit(EXIT_SUCCESS);
}

static int	ft_error_exec(char *cmd)
{
	struct stat *buf;
	int			ret;

	buf = NULL;
	if (!(buf = ft_memalloc(sizeof(struct stat))))
		exit(EXIT_FAILURE);
	ret = lstat(cmd, buf);
	if (ret == -1)
	{
		if (buf)
			free(buf);
		return (ft_display_error_exec(NOFILE, cmd));
	}
	else if ((access(cmd, X_OK)))
	{
		if (buf)
			free(buf);
		return (ft_display_error_exec(PERMDENIED, cmd));
	}
	if (buf)
		free(buf);
	return (FALSE);
}

static void	ft_fils(char **line, t_mini *mini, int env, int ret)
{
	t_cmd *cmd;

	signal(SIGINT, ft_kill);
	ret = EXIT_SUCCESS;
	cmd = ft_search_cmd(mini->cmd, *line);
	if (cmd)
	{
		if (env)
			ret = execve(cmd->path, line, mini->env);
		else
			ret = execve(cmd->path, line, NULL);
	}
	else
	{
		if (ft_strcmp(line[0], ";"))
		{
			if (env)
				ret = execve(*line, line, mini->env);
			else
				ret = execve(*line, line, NULL);
			ft_error_exec(line[0]);
		}
	}
	exit(ret);
}

int			ft_cmd(char **line, t_mini *mini, int env)
{
	pid_t	pid;
	int		stat_loc;
	int		ret;

	pid = fork();
	ret = 0;
	if (pid < 0)
		ft_putendl("Gestion des Erreurs a gerer");
	else if (pid != 0)
	{
		waitpid(pid, &stat_loc, WUNTRACED);
		if (WEXITSTATUS(stat_loc))
			return (FALSE);
		if (WIFEXITED(stat_loc))
			return (TRUE);
		if (WIFSIGNALED(stat_loc))
		{
			ret = WTERMSIG(stat_loc);
			ft_display_error_fils(ret, line[0]);
		}
	}
	else
		ft_fils(line, mini, env, ret);
	return (1);
}
