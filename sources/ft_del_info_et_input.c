/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_info_et_input.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 15:44:57 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:28:30 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>
#include "libft.h"

void		ft_free_operateur(t_operateur *op)
{
	if (op)
	{
		free(op);
		op = NULL;
	}
}

static void	ft_free_input_tab(char **tab)
{
	char **d;

	d = tab;
	while (*tab)
	{
		free(*tab);
		++tab;
	}
	if (d)
		free(d);
}

void		ft_free_input(t_input *input)
{
	if (input)
	{
		ft_free_input(input->next);
		if (input->tab)
			ft_free_input_tab(input->tab);
		free(input);
		input = NULL;
	}
}

void		ft_free_info(t_info_input *info_input)
{
	if (info_input)
	{
		free(info_input);
		info_input = NULL;
	}
}

void		ft_reinit_input(t_mini *mini)
{
	t_info_input *info;

	info = mini->info;
	if (info)
	{
		ft_free_input(info->begin);
		mini->info->begin = NULL;
		mini->info->end = NULL;
		mini->info->size = 0;
	}
}
