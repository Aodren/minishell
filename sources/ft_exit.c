/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:21:24 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:35:35 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>

int		ft_exit(char **tab, t_mini *mini)
{
	tab = NULL;
	ft_destruct_minishell(mini);
	exit(EXIT_SUCCESS);
}
