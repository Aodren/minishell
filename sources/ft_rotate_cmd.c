/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate_cmd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 19:22:41 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 13:36:35 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** rotaion simple droite
** son fils gauche devient son parent et devient son fils droit, le fils droite
** de son nouveau parent devient son fils gauche
*/

void	ft_rotate_simple_rigth(t_cmd *cmd, t_mini *mini)
{
	t_cmd *parent;
	t_cmd *filsgauche;
	t_cmd *filsgauchedroite;

	parent = NULL;
	filsgauche = NULL;
	filsgauchedroite = NULL;
	if (!(filsgauche = cmd->n_left))
		return ;
	filsgauchedroite = filsgauche->n_rigth;
	filsgauche->n_rigth = cmd;
	cmd->n_left = filsgauchedroite;
	parent = cmd->parent;
	cmd->parent = filsgauche;
	filsgauche->parent = parent;
	if (parent)
	{
		if (parent->n_left == cmd)
			parent->n_left = filsgauche;
		else
			parent->n_rigth = filsgauche;
	}
	else
		mini->cmd = cmd;
}

/*
** Rotation simple gauche
** cas ou le noeud a une diference de profondeur des sous arbres de 2 et le fils
** droit une difference de 1
** Rotation gauhe -> Son fils droit devient son parent
** et devient son fils gauche
** le fils gauche de son parent devient sonf fils droit
*/

void	ft_rotate_simple_left(t_cmd *cmd, t_mini *mini)
{
	t_cmd *parent;
	t_cmd *filsdroit;
	t_cmd *filsdroitgauche;

	parent = NULL;
	filsdroit = NULL;
	filsdroitgauche = NULL;
	if (!(filsdroit = cmd->n_rigth))
		return ;
	filsdroitgauche = filsdroit->n_left;
	filsdroit->n_left = cmd;
	cmd->n_rigth = filsdroitgauche;
	parent = cmd->parent;
	cmd->parent = filsdroit;
	filsdroit->parent = parent;
	if (parent)
	{
		if (parent->n_left == cmd)
			parent->n_left = filsdroit;
		else
			parent->n_rigth = filsdroit;
	}
	else
		mini->cmd = cmd;
}
