/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_help.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 19:04:31 by abary             #+#    #+#             */
/*   Updated: 2016/05/21 18:29:18 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_display_help(void)
{
	ft_putendl("minishel :");
	ft_putendl("-c string, use a command written in string");
	ft_putendl("-version, show the version");
	ft_putendl("--help, show this message");
}
