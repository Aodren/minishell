/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_prompt.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 16:03:51 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:49:37 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

void	ft_display_prompt(t_mini *mini)
{
	char *prompt;

	prompt = ft_get_env("MINIPROMPT", mini);
	if (prompt)
		ft_putstr(prompt);
	else
		ft_putstr("minishell> ");
}
