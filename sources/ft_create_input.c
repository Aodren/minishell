/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_input.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 21:07:30 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:26:58 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

/*
** Il faut faire une sorte de split + create de la liste chaine
*/

static int	ft_nbr_words(size_t size, const char *str)
{
	size_t	i;
	int		nbrwords;

	i = 0;
	nbrwords = 0;
	while (i < size)
	{
		while (ft_isspace(*(str + i)))
			++i;
		if (!*(str + i))
			return (nbrwords);
		while (*(str + i) && !ft_isspace(*(str + i)))
			++i;
		while (ft_isspace(*(str + i)))
			++i;
		++nbrwords;
	}
	return (nbrwords);
}

static char	*ft_words(const char **str, size_t *size)
{
	char	*mot;
	char	*d;

	while (**str && ft_isspace(**str))
	{
		++*str;
		--*size;
	}
	d = (char *)*str;
	while (**str && *size && !ft_isspace(**str))
	{
		--*size;
		++*str;
	}
	mot = ft_strndup(d, *str - d);
	return (mot);
}

void		ft_create_input(t_mini *mini, size_t size, const char *str)
{
	int		nbr_words;
	int		i;
	char	**tab;

	nbr_words = ft_nbr_words(size, str);
	tab = ft_memalloc(sizeof(char *) * (nbr_words + 1));
	i = 0;
	while (i < nbr_words)
	{
		tab[i] = ft_words(&str, &size);
		++i;
	}
	tab[i] = NULL;
	ft_lst_add_input(mini, tab, -1);
}
