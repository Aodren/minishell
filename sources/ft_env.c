/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:25:30 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:36:17 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int		ft_env(char **tab, t_mini *mini)
{
	int ret;

	ret = FALSE;
	if (!tab[1])
		ret = ft_printenv(tab, mini);
	else
	{
		if (tab[2] && !ft_strcmp("-i", tab[1]))
			ret = ft_built_in_or_cmd(tab + 2, mini, 0);
		else
			ft_putendl_fd("env : Wrong Arguments", 2);
	}
	return (ret);
}
