/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/03 19:57:35 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:44:13 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <string.h>
#include <stdlib.h>

int		main(int argc, char **argv, char **environ)
{
	t_mini *mini;

	mini = NULL;
	ft_init_minishell(&mini, environ);
	ft_options(argc, argv, mini);
	if (mini->options == C && argc > 2)
		ft_c_options(mini, argv[2]);
	if (mini->options != VERSION && mini->options != HELP)
		ft_minishell(mini);
	else if (mini->options == VERSION)
		ft_display_version();
	else
		ft_display_help();
	ft_destruct_minishell(mini);
	return (EXIT_SUCCESS);
}
