/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 14:23:19 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:39:39 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <dirent.h>

static char	*ft_create_path(const char *path, const char *name)
{
	char	*bin;
	char	*d;
	size_t	size_path;
	size_t	size_name;

	size_name = ft_strlen(name);
	size_path = ft_strlen(path);
	bin = ft_strnew(size_name + size_path + 1);
	bin = ft_strcpy(bin, path);
	*(bin + size_path) = '/';
	d = bin;
	bin = ft_strcpy(bin + size_path + 1, name);
	return (d);
}

static void	ft_open_path(const char *path, t_mini *mini)
{
	DIR				*dirp;
	struct dirent	*dp;
	char			*bin;

	bin = NULL;
	if (!(dirp = opendir(path)))
		return ;
	while ((dp = readdir(dirp)))
	{
		bin = ft_create_path(path, dp->d_name);
		ft_create_cmd(bin, mini, dp->d_name);
		free(bin);
		bin = NULL;
	}
	closedir(dirp);
}

static void	ft_parcour_path(const char *path, t_mini *mini)
{
	char	*ret;
	size_t	d;
	char	*folder;

	ret = NULL;
	d = 1;
	while (*path)
	{
		ret = ft_strchr(path, ':');
		d = ret - path;
		if (ret)
		{
			folder = ft_strndup(path, d);
			ft_open_path(folder, mini);
			free(folder);
		}
		else
		{
			folder = ft_strdup(path);
			ft_open_path(folder, mini);
			free(folder);
			break ;
		}
		path += d + 1;
	}
}

void		ft_find_path(t_mini *mini)
{
	size_t i;

	i = 0;
	while (i < mini->size_env)
	{
		if (mini->env[i] && ft_strchrstart(mini->env[i], "PATH="))
		{
			ft_parcour_path(mini->env[i] + 5, mini);
			break ;
		}
		++i;
	}
}
