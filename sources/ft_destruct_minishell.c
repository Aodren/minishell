/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destruct_minishell.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 19:10:30 by abary             #+#    #+#             */
/*   Updated: 2016/05/05 15:58:35 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>

static void	ft_destruct_env(char **env)
{
	char **d;

	d = env;
	while (*env)
	{
		free(*env);
		*env = NULL;
		++env;
	}
	free(d);
	d = NULL;
}

void		ft_destruct_minishell(t_mini *mini)
{
	ft_destruct_env(mini->env);
	ft_del_tree(mini->cmd);
	ft_free_operateur(mini->operateur);
	ft_free_input(mini->info->begin);
	ft_free_info(mini->info);
	free(mini->bulletin);
	free(mini);
}
