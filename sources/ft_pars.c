/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pars.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 14:30:11 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 17:53:21 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

void	ft_recup_input(const char *line, t_mini *mini)
{
	char	*d;

	d = NULL;
	while (*line)
	{
		if (!*line)
			return ;
		d = (char *)line;
		line = ft_add_elem(mini, d);
		if (!*line)
			return ;
	}
}

int		ft_verif_line(t_mini *mini)
{
	t_input *input;

	input = NULL;
	if (mini->info && mini->info->begin)
		input = mini->info->begin;
	else
		return (0);
	if (input->op >= 0)
		return (0);
	while (input)
	{
		if (input->op >= 0 && input->op != 2)
		{
			if (!input->next || input->next->op >= 0)
				return (0);
		}
		else if (input->op == 2 && input->next && input->next->op >= 0)
			return (0);
		input = input->next;
	}
	return (1);
}

int		ft_empty_line(const char *line)
{
	while (*line)
	{
		if (!ft_isspace(*line))
			return (0);
		++line;
	}
	return (1);
}
