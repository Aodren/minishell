/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 20:07:44 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:23:58 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
** Realloue le tableau env pour y ajouter le nouvel env
** dans mini->size_env on a la taille avec le null y compris
*/

char	**ft_realloc_env(char **env, t_mini *mini, char *add_env)
{
	size_t	nbr;
	size_t	i;
	char	**new_env;

	i = -1;
	nbr = mini->size_env;
	mini->size_env++;
	new_env = ft_memalloc(sizeof(char *) * nbr + 1);
	while (++i < nbr)
		new_env[i] = env[i];
	new_env[i - 1] = ft_strdup(add_env);
	new_env[i] = NULL;
	free(env);
	return (new_env);
}
