/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fin_commande.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 12:55:50 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:30:43 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

t_input	*ft_fin_commande(t_mini *mini, t_input *input, int *etat)
{
	*etat = NEWCMD;
	*etat = ft_built_in_or_cmd(input->next->tab, mini, 1);
	return (input->next->next);
}
