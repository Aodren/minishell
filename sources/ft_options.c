/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 21:18:04 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:24:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
** Parse les options :
** -c string On lis la commande et on l'execute; on quite bash
** -version pour afficher la version du logiciel
** -file(INIT) pour init avec un ficher de configuration PATH modifer etc ...
** --help pour afficher les options
*/

void		ft_options(int argc, char **argv, t_mini *mini)
{
	int ret;

	if (argc == 1)
		return ;
	while (argc > 1)
	{
		if ((ret = ft_recup_options(*++argv, mini)) <= 0)
		{
			ft_display_error(INVALID_ARG);
			exit(EXIT_SUCCESS);
		}
		else
			return ;
		argc--;
	}
}

void		ft_c_options(t_mini *mini, char *argv)
{
	ft_recup_input(argv, mini);
	if (!ft_verif_line(mini))
		ft_putendl("Probleme de parse");
	else if (mini->info->begin)
	{
		ft_pars_line(mini);
		ft_launch_cmd(mini);
	}
	ft_reinit_input(mini);
	ft_exit(&argv, mini);
}
