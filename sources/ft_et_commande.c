/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_et_commande.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 13:47:33 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:48:28 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_input	*ft_et_commande(t_mini *mini, t_input *input, int *etat)
{
	int ret;

	ret = ft_built_in_or_cmd(input->next->tab, mini, 1);
	if (!ret)
		*etat = 0;
	return (input->next->next);
}
