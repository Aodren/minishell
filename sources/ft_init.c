/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:05:17 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:34:49 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
**	On enregistre la taille de l'environnement en plus
**  Si pas d'environnement, il faut faire un tableau avec NULL
*/

int			ft_init_pwd(t_mini *mini)
{
	char	*pwd;

	pwd = ft_get_env("PWD", mini);
	if (pwd)
	{
		pwd = ft_strjoin("OLDPWD=", pwd);
		mini->env = ft_del_env(mini->env, mini, "OLDPWD");
		mini->env = ft_realloc_env(mini->env, mini, pwd);
	}
	else
	{
		ft_set_pwd(mini);
		ft_init_pwd(mini);
	}
	free(pwd);
	return (1);
}

static void	ft_init_prompt(t_mini *mini)
{
	mini->env = ft_del_env(mini->env, mini, "PROMPT");
	mini->env = ft_del_env(mini->env, mini, "MINIPROMPT");
	mini->env = ft_realloc_env(mini->env, mini, "MINIPROMPT=minishell> ");
}

static char	**ft_init_env_info(char **environ, t_mini **mini)
{
	int		nbr;
	int		i;
	char	**env;

	nbr = 0;
	i = 0;
	while (environ[nbr])
		++nbr;
	(*mini)->size_env = nbr + 1;
	if (!(env = ft_memalloc(sizeof(char *) * nbr + 1)))
		exit(EXIT_FAILURE);
	while (i < nbr)
	{
		env[i] = ft_strdup(environ[i]);
		++i;
	}
	env[i] = NULL;
	return (env);
}

void		ft_init_minishell(t_mini **mini, char **environ)
{
	if (!(*mini = ft_memalloc(sizeof(t_mini))))
		exit(EXIT_FAILURE);
	(*mini)->options = 0;
	(*mini)->env = NULL;
	(*mini)->size_env = 0;
	(*mini)->env = ft_init_env_info(environ, mini);
	(*mini)->bulletin = NULL;
	(*mini)->bulletin = ft_init_bulletins();
	(*mini)->cmd = NULL;
	(*mini)->operateur = NULL;
	(*mini)->operateur = ft_init_operateur();
	(*mini)->info = NULL;
	(*mini)->info = ft_init_info_input();
	ft_find_path(*mini);
	ft_init_prompt(*mini);
	ft_init_pwd(*mini);
}

t_cmd		*ft_create_node(t_cmd *cmd, const char *path, const char *name)
{
	cmd = ft_memalloc(sizeof(t_cmd));
	cmd->n_rigth = NULL;
	cmd->n_left = NULL;
	cmd->parent = NULL;
	cmd->cmd = ft_strdup(name);
	cmd->path = ft_strdup(path);
	cmd->equilibre = 0;
	return (cmd);
}
