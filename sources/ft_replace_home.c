/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace_home.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 20:09:15 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:41:39 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
** Juste gerer le ~/
*/

static size_t	ft_calcul_new_cd(char *str, char *home)
{
	size_t	nb;
	size_t	size_home;

	nb = ft_strlen(str);
	if (!home)
		return (0);
	size_home = ft_strlen(home) - 1;
	if (*str == '~')
		nb += size_home;
	return (nb);
}

static char		*ft_replace_string(char *str, char *d, char *home)
{
	char	*save;
	size_t	size_home;
	int		ok;

	save = d;
	size_home = ft_strlen(home);
	ok = 1;
	while (*str)
	{
		if (ok && *str == '~')
		{
			d = ft_strcat(d, home);
			++str;
			d += size_home;
			ok = 0;
			continue;
		}
		else
			*d = *str;
		++str;
		++d;
	}
	d = save;
	return (d);
}

char			*ft_replace_home(char *str, t_mini *mini)
{
	size_t	nb;
	char	*d;
	char	*home;

	home = ft_get_env("HOME", mini);
	if (!home)
		return (str);
	nb = ft_calcul_new_cd(str, home);
	if (nb == ft_strlen(str))
		return (str);
	d = ft_memalloc(sizeof(char ) * (nb + 1));
	d = ft_replace_string(str, d, home);
	free(str);
	return (d);
}
