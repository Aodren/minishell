/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace_cd_pwd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 20:49:45 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:43:33 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
** Il faut touver la premiere occurence pour remplacer
*/

static char		*ft_find_in_pwd(char *str, char *pwd, char *replace, char *new)
{
	char	*d;
	char	*save;

	if (!(d = ft_strstr(str, pwd)))
		return (NULL);
	save = new;
	while (*str)
	{
		if (str == d)
		{
			while (*replace)
				*new++ = *replace++;
			while (*pwd)
			{
				++pwd;
				++str;
			}
			continue ;
		}
		*new = *str;
		++str;
		++new;
	}
	return (new = save);
}

char			*ft_replace_dd_pwd(char *pwd, char *replace, t_mini *mini)
{
	size_t	taille;
	char	*pwd_env;
	char	*str;

	str = NULL;
	if (!(pwd_env = ft_get_env("PWD", mini)))
		return (replace);
	if (pwd_env)
		taille = ft_strlen(pwd_env) + ft_strlen(replace) - ft_strlen(pwd);
	else
		return (pwd);
	str = ft_memalloc(sizeof(char) * (taille + 1));
	ft_find_in_pwd(pwd_env, pwd, replace, str);
	return (str);
}
