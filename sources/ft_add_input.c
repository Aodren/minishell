/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_input.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 16:54:54 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:40:10 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

static int		ft_strequ_one_op(const char *str, const char *op)
{
	while (*str && *str == *op && *op)
	{
		++str;
		++op;
	}
	if (!*op)
		return (1);
	return (0);
}

static int		ft_strequ_op(const char *str, t_operateur *op)
{
	int i;

	i = 0;
	while (i < T_OPERATEUR)
	{
		if (ft_strequ_one_op(str, op[i++].operateur))
			return (i - 1);
	}
	return (-1);
}

char			*ft_add_elem(t_mini *mini, const char *line)
{
	char		*d;
	t_operateur *op;
	int			indice;

	while (ft_isspace(*line))
		++line;
	d = (char *)line;
	op = mini->operateur;
	while (*line)
	{
		if (*line == '\\')
			line = line + 2;
		if ((indice = ft_strequ_op(line, op)) != -1)
		{
			if (line - d)
				ft_create_input(mini, line - d, d);
			line = ft_lst_add_operateur(mini, indice, line);
			return ((char *)line);
		}
		++line;
	}
	ft_create_input(mini, line - d, d);
	return ((char *)line);
}
