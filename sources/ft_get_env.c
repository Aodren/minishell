/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 16:13:15 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:29:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int		ft_search_path(char *line, t_mini *mini)
{
	size_t	size;
	char	**env;
	size_t	size_env;

	size = 0;
	env = mini->env;
	while (*(line + size) && *(line + size) != '=')
		++size;
	if (!*(line + size))
		return (-1);
	while (*env)
	{
		size_env = ft_strlen(*env);
		if (!ft_strncmp(*env, line, size) && size_env > size &&
				*(*env + size) == '=')
			return (1);
		++env;
	}
	return (0);
}

/*
** Permet d'obtenir un element de l'env, renvoi un pointeur sur la variable
** d'env;
*/

char	*ft_get_env(char *str, t_mini *mini)
{
	char	**env;
	size_t	size_env;
	size_t	size_str;

	env = mini->env;
	size_str = ft_strlen(str);
	while (*env)
	{
		size_env = ft_strlen(*env);
		if (ft_strchrstart(*env, str) && size_env > size_str
				&& *(*env + size_str) == '=')
			return ((*env + size_str + 1));
		++env;
	}
	return (NULL);
}

void	ft_print_all_env(t_mini *mini)
{
	char **env;

	env = mini->env;
	while (*env)
	{
		ft_putendl(*env);
		++env;
	}
}

char	*ft_get_name_env(char *str, t_mini *mini)
{
	size_t	size;
	char	**env;
	size_t	size_env;
	size_t	ret;

	size = 0;
	ret = 0;
	env = mini->env;
	while (*(str + size) && *(str + size) != '=')
	{
		++size;
		++ret;
	}
	if (!*(str + size))
		return (NULL);
	while (*env)
	{
		size_env = ft_strlen(*env);
		if (!ft_strncmp(*env, str, size) && size_env > size &&
				*(*env + size) == '=')
			return (ft_strndup(str, ret));
		++env;
	}
	return (NULL);
}
