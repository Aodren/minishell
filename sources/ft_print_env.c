/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/06 19:06:20 by abary             #+#    #+#             */
/*   Updated: 2016/05/12 19:51:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int		ft_printenv(char **tab, t_mini *mini)
{
	char *env;

	if (*(tab + 1))
	{
		if ((env = (ft_get_env(*(tab + 1), mini))))
		{
			ft_putendl(env);
			return (1);
		}
		else
			return (0);
	}
	ft_print_all_env(mini);
	return (1);
}
