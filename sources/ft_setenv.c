/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:27:22 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 16:20:01 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

static int	ft_del_old_env(t_mini *mini, char *name)
{
	int ret;

	ret = 0;
	mini->env = ft_del_env(mini->env, mini, name);
	if (!ft_strcmp(name, "PATH"))
	{
		ft_del_tree(mini->cmd);
		mini->cmd = NULL;
		ret = 1;
	}
	free(name);
	return (ret);
}

static int	ft_ret_set_env(void)
{
	ft_putendl_fd("setenv : wrong number of arguments", 2);
	return (0);
}

int			ft_setenv(char **tab, t_mini *mini)
{
	int		ret;
	char	*name;

	if (!tab[1] || tab[2])
		ft_ret_set_env();
	else if ((ret = ft_search_path(tab[1], mini)) == 1)
	{
		name = ft_get_name_env(tab[1], mini);
		ret = 0;
		if (name)
			ret = ft_del_old_env(mini, name);
		mini->env = ft_realloc_env(mini->env, mini, tab[1]);
		if (ret)
			ft_find_path(mini);
		return (1);
	}
	else if (ret == -1)
	{
		ft_putendl_fd("setenv missing \"=\"", 2);
		return (0);
	}
	else
		mini->env = ft_realloc_env(mini->env, mini, tab[1]);
	return (1);
}
