/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_tree.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 15:36:13 by abary             #+#    #+#             */
/*   Updated: 2016/05/05 15:44:07 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>

void	ft_del_tree(t_cmd *cmd)
{
	if (cmd)
	{
		ft_del_tree(cmd->n_rigth);
		ft_del_tree(cmd->n_left);
		if (cmd->cmd)
		{
			free(cmd->cmd);
			cmd->cmd = NULL;
		}
		if (cmd->path)
		{
			free(cmd->path);
			cmd->path = NULL;
		}
		free(cmd);
	}
}
