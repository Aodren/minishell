/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ou_commande.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/12 14:41:36 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:21:52 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

t_input	*ft_ou_commande(t_mini *mini, t_input *input, int *etat)
{
	int ret;

	if (*etat == 1)
		return (input->next->next);
	ret = ft_built_in_or_cmd(input->next->tab, mini, 1);
	if (ret)
		*etat = 1;
	else
		*etat = 0;
	return (input->next->next);
}
