/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_operande.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 14:40:25 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:28:54 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <stdlib.h>

static void	ft_fill_operateur(t_operateur *operateur)
{
	int i;

	i = 0;
	operateur[i].operateur = "||";
	operateur[i].size = 2;
	operateur[i++].op = ft_ou_commande;
	operateur[i].operateur = "&&";
	operateur[i].size = 2;
	operateur[i++].op = ft_et_commande;
	operateur[i].operateur = ";";
	operateur[i].size = 1;
	operateur[i++].op = ft_fin_commande;
	operateur[i].operateur = ">";
	operateur[i].size = 1;
	operateur[i++].op = NULL;
	operateur[i].operateur = "<";
	operateur[i].size = 1;
	operateur[i++].op = NULL;
	operateur[i].operateur = "|";
	operateur[i].size = 1;
	operateur[i++].op = NULL;
	operateur[i].operateur = "&";
	operateur[i].size = 1;
	operateur[i++].op = NULL;
}

t_operateur	*ft_init_operateur(void)
{
	t_operateur *operateur;

	if (!(operateur = ft_memalloc(sizeof(t_operateur) * T_OPERATEUR)))
		exit(EXIT_FAILURE);
	ft_fill_operateur(operateur);
	return (operateur);
}
