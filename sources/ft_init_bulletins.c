/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_bulletins.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:14:56 by abary             #+#    #+#             */
/*   Updated: 2016/05/12 18:21:50 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

static void		ft_attrib_bulletins(t_bulletin *bulletin)
{
	int i;

	i = 0;
	bulletin[i].bulletin = "cd";
	bulletin[i++].f = ft_cd;
	bulletin[i].bulletin = "exit";
	bulletin[i++].f = ft_exit;
	bulletin[i].bulletin = "env";
	bulletin[i++].f = ft_env;
	bulletin[i].bulletin = "setenv";
	bulletin[i++].f = ft_setenv;
	bulletin[i].bulletin = "unsetenv";
	bulletin[i++].f = ft_unsetenv;
	bulletin[i].bulletin = "printenv";
	bulletin[i++].f = ft_printenv;
	bulletin[i].bulletin = NULL;
	bulletin[i++].f = NULL;
}

t_bulletin		*ft_init_bulletins(void)
{
	t_bulletin *bulletin;

	if (!(bulletin = ft_memalloc(sizeof(t_bulletin) * T_BULLETIN)))
		exit(EXIT_FAILURE);
	ft_attrib_bulletins(bulletin);
	return (bulletin);
}
