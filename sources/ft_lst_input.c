/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_input.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 21:59:43 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:23:36 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <stdlib.h>

/*
** Va falloir free apres chaque commande
*/

static void	ft_push_back_input(t_info_input *info, t_input *new)
{
	if (!info->begin)
	{
		info->begin = new;
		info->end = new;
	}
	else
	{
		info->end->next = new;
		info->end = new;
	}
}

void		ft_lst_add_input(t_mini *mini, char **tab, int operande)
{
	t_input			*new;
	t_info_input	*info;

	info = mini->info;
	new = ft_init_lst_input(tab, info, operande);
	ft_push_back_input(info, new);
}

char		*ft_lst_add_operateur(t_mini *mini, int indice, const char *line)
{
	t_operateur		*op;
	char			**tab;

	op = mini->operateur;
	if (!(tab = ft_memalloc(sizeof(char *) * (2))))
		exit(EXIT_FAILURE);
	tab[1] = NULL;
	tab[0] = ft_strdup(op[indice].operateur);
	line += op[indice].size;
	ft_lst_add_input(mini, tab, indice);
	return ((char *)line);
}
