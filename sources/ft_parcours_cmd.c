/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parcours_cmd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 16:50:41 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 16:07:31 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

void	ft_parcours_cmd(t_cmd *cmd)
{
	if (cmd)
	{
		ft_putnbr(cmd->equilibre);
		ft_putstr(" ");
		ft_putendl(cmd->cmd);
		ft_parcours_cmd(cmd->n_left);
		ft_parcours_cmd(cmd->n_rigth);
	}
}

t_cmd	*ft_search_cmd(t_cmd *cmd, char *line)
{
	int ret;

	ret = 0;
	if (cmd)
	{
		if ((ret = ft_strcmp(cmd->cmd, line)) == 0)
			return (cmd);
		else if (ret > 0)
			return (ft_search_cmd(cmd->n_left, line));
		else
			return (ft_search_cmd(cmd->n_rigth, line));
	}
	return (NULL);
}
