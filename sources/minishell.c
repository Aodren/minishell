/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/19 21:04:17 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 17:53:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <stdlib.h>

void	ft_pars_line(t_mini *mini)
{
	t_input *input;
	char	**tab;

	input = mini->info->begin;
	while (input)
	{
		tab = input->tab;
		while (*tab)
		{
			*tab = ft_replace_home(*tab, mini);
			*tab = ft_replace_cara(*tab);
			++tab;
		}
		input = input->next;
	}
}

void	ft_sig_int(int s)
{
	signal(s, ft_sig_int);
}

int		ft_minishell(t_mini *mini)
{
	char		*line;
	t_bulletin	*bulletin;

	line = NULL;
	signal(SIGINT, ft_sig_int);
	bulletin = mini->bulletin;
	ft_display_prompt(mini);
	while (get_next_line(STDIN, &line))
	{
		ft_recup_input(line, mini);
		if (ft_empty_line(line) || !ft_verif_line(mini))
			ft_putchar('\0');
		else if (mini->info->begin)
		{
			ft_pars_line(mini);
			ft_launch_cmd(mini);
		}
		ft_reinit_input(mini);
		free(line);
		ft_display_prompt(mini);
	}
	if (line)
		free(line);
	return (1);
}
