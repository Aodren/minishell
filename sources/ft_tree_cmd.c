/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tree_cmd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 16:54:52 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:39:09 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <sys/stat.h>

/*
** Il faut faire un arbre AVL car moins rapide en insertion et suppression
** qu'in rouge et noir ou arbre binaire simple
** mais plus opti en recherche
*/

void			ft_create_root(const char *path, t_mini *mini, const char *name)
{
	t_cmd *cmd;

	cmd = NULL;
	cmd = ft_create_node(cmd, path, name);
	mini->cmd = cmd;
}

static size_t	ft_calcul_profondeur(t_cmd *cmd)
{
	int nbr1;
	int nbr2;

	if (!cmd)
		return (0);
	nbr1 = 1 + ft_calcul_profondeur(cmd->n_left);
	nbr2 = 1 + ft_calcul_profondeur(cmd->n_rigth);
	if (nbr1 > nbr2)
		return (nbr1);
	else
		return (nbr2);
}

/*
** Requilibrage de l'arbre
*/

static void		ft_balance(t_cmd *cmd, t_mini *mini)
{
	int nbr;

	if (!cmd || cmd->n_rigth || cmd->n_left)
		return ;
	nbr = ft_calcul_profondeur(cmd->n_rigth) -
		ft_calcul_profondeur(cmd->n_left);
	if (nbr == 2)
	{
		nbr = ft_calcul_profondeur(cmd->n_rigth->n_rigth) -
		ft_calcul_profondeur(cmd->n_rigth->n_left);
		if (nbr == -1)
			ft_rotate_simple_rigth(cmd, mini);
		ft_rotate_simple_left(cmd, mini);
	}
	else if (nbr == -2)
	{
		nbr = ft_calcul_profondeur(cmd->n_left->n_rigth) -
		ft_calcul_profondeur(cmd->n_left->n_left);
		if (nbr == 1)
			ft_rotate_simple_left(cmd, mini);
		ft_rotate_simple_rigth(cmd, mini);
	}
	ft_balance(cmd->parent, mini);
}

void			ft_add_leaf(t_cmd *cmd, t_cmd *new, t_mini *mini)
{
	if (cmd)
	{
		if (ft_strcmp(new->cmd, cmd->cmd) <= 0)
		{
			if (cmd->n_left)
				ft_add_leaf(cmd->n_left, new, mini);
			else if ((cmd->n_left = new))
			{
				cmd->n_left->parent = cmd;
				return ;
			}
		}
		else
		{
			if (cmd->n_rigth)
				ft_add_leaf(cmd->n_rigth, new, mini);
			else
			{
				cmd->n_rigth = new;
				cmd->n_rigth->parent = cmd;
				return ;
			}
		}
		ft_balance(cmd, mini);
	}
}

void			ft_create_cmd(const char *path, t_mini *mini, char *name)
{
	int			ret;
	struct stat buf;
	t_cmd		*new;

	ret = 0;
	new = NULL;
	if ((ret = lstat(path, &buf)) == -1)
		return ;
	if (((buf.st_mode & S_IFREG) == S_IFREG)
			&& ((buf.st_mode & S_IXUSR) == S_IXUSR))
	{
		if (!mini->cmd)
			ft_create_root(path, mini, name);
		else
		{
			new = ft_create_node(new, path, name);
			ft_add_leaf(mini->cmd, new, mini);
			ft_balance(new, mini);
		}
	}
}
