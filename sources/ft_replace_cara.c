/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace_cara.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/21 19:22:04 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:35:05 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

static size_t	ft_nbr_backspace(const char *str)
{
	size_t nbr;

	nbr = 0;
	while (*str)
	{
		if (*str == '\\' && *(str + 1))
			++nbr;
		++str;
	}
	return (nbr);
}

static char		*ft_replace(const char *tab, char *ret)
{
	char *d;

	d = ret;
	while (*tab)
	{
		if (*tab == '\\' && *(tab + 1))
			++tab;
		*ret++ = *tab++;
	}
	return (d);
}

char			*ft_replace_cara(char *tab)
{
	char	*ret;
	size_t	nbr;

	if (!(nbr = ft_nbr_backspace(tab)))
		return (tab);
	ret = ft_memalloc(sizeof(char *) * (ft_strlen(tab) - nbr + 1));
	ret = ft_replace(tab, ret);
	return (ret);
}
