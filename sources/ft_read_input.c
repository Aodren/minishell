/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 21:32:17 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:17:39 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int			ft_built_in_or_cmd(char **tab, t_mini *mini, int env)
{
	t_bulletin	*bulletin;
	int			i;
	int			ret;

	ret = 0;
	bulletin = mini->bulletin;
	i = 0;
	while (bulletin[i].f != NULL)
	{
		if ((ret = ft_strchrstart(*tab, bulletin[i].bulletin)))
		{
			ret = bulletin[i].f(tab, mini);
			return (ret);
		}
		++i;
	}
	if (!ret)
		ret = ft_cmd(tab, mini, env);
	return (ret);
}

static int	ft_launch_one_cmd(t_mini *mini)
{
	t_input *input;

	input = mini->info->begin;
	return (ft_built_in_or_cmd(input->tab, mini, 1));
}

void		ft_launch_cmd(t_mini *mini)
{
	t_info_input	*info;
	t_input			*input;
	int				etat;

	etat = -1;
	info = mini->info;
	if (info->size > 1)
	{
		etat = ft_launch_one_cmd(mini);
		input = info->begin->next;
		while (input)
		{
			if (!(input = ft_minishell_launch_op(&etat, input, mini)))
				return ;
		}
	}
	else
		ft_launch_one_cmd(mini);
}
