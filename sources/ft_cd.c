/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:24:09 by abary             #+#    #+#             */
/*   Updated: 2016/05/24 17:28:16 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libft.h>

/*
** attention au segfault /tmp
*/

static char		*ft_cd_minus(char *str, t_mini *mini)
{
	char *old_pwd;

	old_pwd = NULL;
	if ((ft_strcmp(str, "-")))
		return (str);
	old_pwd = ft_get_env("OLDPWD", mini);
	return (old_pwd);
}

static int		ft_launch_cd(char *cmd)
{
	int				ret;
	DIR				*dirp;
	struct stat		*buf;

	dirp = NULL;
	buf = NULL;
	if ((ret = chdir(cmd)) == 0)
		return (1);
	if (!(buf = ft_memalloc(sizeof(struct stat))))
		exit(EXIT_FAILURE);
	ret = lstat(cmd, buf);
	if (ret == -1)
		return (ft_display_error_cd(NOFILE, cmd, buf));
	else if ((buf->st_mode & S_IFDIR) != S_IFDIR)
		return (ft_display_error_cd(NODIR, cmd, buf));
	else if (access(cmd, X_OK) == -1)
		return (ft_display_error_cd(PERMDENIED, cmd, buf));
	else
		return (ft_display_error_cd(ERROROCCUR, cmd, buf));
	if (buf)
		free(buf);
	return (1);
}

void			ft_set_pwd(t_mini *mini)
{
	char *pwd;
	char *d;

	pwd = NULL;
	pwd = getcwd(NULL, 0);
	d = pwd;
	pwd = ft_strjoin("PWD=", pwd);
	if (d)
		free(d);
	else
		return ;
	mini->env = ft_del_env(mini->env, mini, "PWD");
	mini->env = ft_realloc_env(mini->env, mini, pwd);
	if (pwd)
		free(pwd);
}

static	int		ft_ret_cd(int alloc, char *cmd, int ret)
{
	if (alloc && cmd)
		free(cmd);
	return (ret);
}

int				ft_cd(char **tab, t_mini *mini)
{
	char	*cmd;
	int		ret;
	int		alloc;

	alloc = 0;
	cmd = NULL;
	if (!*(tab + 1) || (*(tab + 2) && *(tab + 3)))
		return (0);
	if (*(tab + 2))
	{
		if (!ft_get_env("PWD", mini))
			return (ft_display_error_cd(NOPWD, NULL, NULL));
		cmd = ft_replace_dd_pwd(tab[1], tab[2], mini);
		alloc = 1;
	}
	else
		cmd = ft_cd_minus(tab[1], mini);
	if (cmd)
		ret = ft_launch_cd(cmd);
	else
		return (ft_ret_cd(alloc, cmd, -1));
	if (ret == 1 && ft_init_pwd(mini))
		ft_set_pwd(mini);
	return (ft_ret_cd(alloc, cmd, ret));
}
