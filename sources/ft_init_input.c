/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/05 15:28:07 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:46:52 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

t_info_input	*ft_init_info_input(void)
{
	t_info_input *info;

	if (!(info = ft_memalloc(sizeof(t_info_input))))
		exit(EXIT_SUCCESS);
	info->begin = NULL;
	info->end = NULL;
	info->size = 0;
	return (info);
}

t_input			*ft_init_lst_input(char **tab, t_info_input *info, int operande)
{
	t_input *new;

	if (!(new = ft_memalloc(sizeof(t_input))))
		exit(EXIT_FAILURE);
	new->tab = tab;
	new->next = NULL;
	new->op = operande;
	info->size++;
	return (new);
}
