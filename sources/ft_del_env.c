/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_del_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 15:07:34 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 15:38:13 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

/*
** Realloue le tableau avec le tableau en moins
*/

static char	**ft_del_realloc(char **env, char **new_env, t_mini *mini)
{
	size_t i;
	size_t nbr;

	i = 0;
	nbr = 0;
	new_env = ft_memalloc(sizeof(char *) * mini->size_env);
	while (i < mini->size_env)
	{
		if (env[i])
			new_env[nbr++] = env[i];
		++i;
	}
	new_env[nbr] = NULL;
	mini->size_env--;
	free(env);
	env = NULL;
	return (new_env);
}

/*
** Retire un element du tableau env
*/

char		**ft_del_env(char **env, t_mini *mini, char *del_env)
{
	char	**new_env;
	size_t	i;
	size_t	size_del;
	size_t	my_env;

	i = -1;
	size_del = ft_strlen(del_env);
	new_env = NULL;
	while (++i < mini->size_env - 1)
	{
		my_env = ft_strlen(env[i]);
		if (ft_strchrstart(env[i], del_env) && my_env > size_del &&
				env[i][size_del] == '=')
		{
			free(env[i]);
			env[i] = NULL;
			break ;
		}
	}
	if (i != mini->size_env - 1)
	{
		new_env = ft_del_realloc(env, new_env, mini);
		return (new_env);
	}
	return (env);
}
