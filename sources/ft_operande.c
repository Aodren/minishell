/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operande.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/08 12:03:15 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 14:26:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

t_input		*ft_minishell_launch_op(int *etat, t_input *input, t_mini *mini)
{
	t_operateur *op;
	int			i;

	op = mini->operateur;
	i = -1;
	if (!input->next)
	{
		ft_built_in_or_cmd(input->tab, mini, 1);
		return (NULL);
	}
	while (++i < T_OPERATEUR)
	{
		if (!ft_strcmp(input->tab[0], op[i].operateur))
		{
			if (op[i].op == NULL)
			{
				ft_putstr_fd(op[i].operateur, 2);
				ft_putendl_fd(": option non gerer", 2);
				return (NULL);
			}
			input = op[i].op(mini, input, etat);
			return (input);
		}
	}
	return (input);
}
