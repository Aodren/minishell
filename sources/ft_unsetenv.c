/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 20:28:37 by abary             #+#    #+#             */
/*   Updated: 2016/05/22 16:08:02 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

int		ft_unsetenv(char **env, t_mini *mini)
{
	if (!env[1] || env[2])
	{
		ft_putendl_fd("unsetenv : wrong number of arguments", 2);
		return (0);
	}
	else if ((ft_get_env(env[1], mini)))
	{
		mini->env = ft_del_env(mini->env, mini, env[1]);
		if (!ft_strcmp(env[1], "PATH"))
		{
			ft_del_tree(mini->cmd);
			mini->cmd = NULL;
		}
		return (1);
	}
	else
	{
		ft_putstr_fd("unsetenv: ", 2);
		ft_putstr_fd(env[1], 2);
		ft_putendl_fd(" env not found", 2);
	}
	return (0);
}
